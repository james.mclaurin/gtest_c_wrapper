
/*
Helper file for Google Test To interface to C.
Source Referenced: https://www.distek.com/blog/using-google-test-unit-test-c-code/
*/

#include <gtest\gtest.h>

extern "C"
{
#include "gTest_helper.h"
}

// Fatal Assertions
extern "C" void AssertTrue(unsigned int expected)
{
	ASSERT_TRUE(expected != 0);
}

extern "C" void AssertFalse(unsigned int expected)
{
	ASSERT_FALSE(expected == 0);
}

extern "C" void AssertEq(unsigned int expected, unsigned int actual)
{
	ASSERT_EQ(expected, actual);
}

extern "C" void AssertNe(unsigned int expected, unsigned int actual)
{
	ASSERT_NE(expected, actual);
}

extern "C" void AssertLt(unsigned int expected, unsigned int actual)
{
	ASSERT_LT(expected, actual);
}

extern "C" void AssertLe(unsigned int expected, unsigned int actual)
{
	ASSERT_LE(expected, actual);
}

extern "C" void AssertGt(unsigned int expected, unsigned int actual)
{
	ASSERT_GT(expected, actual);
}

extern "C" void AssertGe(unsigned int expected, unsigned int actual)
{
	ASSERT_GE(expected, actual);
}


// Nonfatal Assertions
extern "C" void ExpectTrue(unsigned int expected)
{
	EXPECT_TRUE(expected != 0);
}

extern "C" void ExpectFalse(unsigned int expected)
{
	EXPECT_FALSE(expected == 0);
}

extern "C" void ExpectEq(unsigned int expected, unsigned int actual)
{
	EXPECT_EQ(expected, actual);
}

extern "C" void ExpectNe(unsigned int expected, unsigned int actual)
{
	EXPECT_NE(expected, actual);
}

extern "C" void ExpectLt(unsigned int expected, unsigned int actual)
{
	EXPECT_LT(expected, actual);
}

extern "C" void ExpectLe(unsigned int expected, unsigned int actual)
{
	EXPECT_LE(expected, actual);
}

extern "C" void ExpectGt(unsigned int expected, unsigned int actual)
{
	EXPECT_GT(expected, actual);
}

extern "C" void ExpectGe(unsigned int expected, unsigned int actual)
{
	EXPECT_GE(expected, actual);
}
