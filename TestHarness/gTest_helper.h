
/*
	Helper file for Google Test To interface to C.
	Source Referenced: https://www.distek.com/blog/using-google-test-unit-test-c-code/
*/

#ifndef GTEST_HELPER_H
#define GTEST_HELPER_H

// Fatal Assertions
extern void AssertTrue(unsigned int expected);
extern void AssertFalse(unsigned int expected);
extern void AssertEq(unsigned int expected, unsigned int actual);
extern void AssertNe(unsigned int expected, unsigned int actual);
extern void AssertLt(unsigned int expected, unsigned int actual);
extern void AssertLe(unsigned int expected, unsigned int actual);
extern void AssertGt(unsigned int expected, unsigned int actual);
extern void AssertGe(unsigned int expected, unsigned int actual);

// Nonfatal Assertions
extern void ExpectTrue(unsigned int expected);
extern void ExpectFalse(unsigned int expected);
extern void ExpectEq(unsigned int expected, unsigned int actual);
extern void ExpectNe(unsigned int expected, unsigned int actual);
extern void ExpectLt(unsigned int expected, unsigned int actual);
extern void ExpectLe(unsigned int expected, unsigned int actual);
extern void ExpectGt(unsigned int expected, unsigned int actual);
extern void ExpectGe(unsigned int expected, unsigned int actual);

#endif