#include <gtest\gtest.h>
#include <Windows.h>
#include <stdio.h>

int wmain(int argc, wchar_t* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}